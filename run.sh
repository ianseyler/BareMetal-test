#!/bin/bash
# from http://unix.stackexchange.com/questions/9804/how-to-comment-multi-line-commands-in-shell-scripts

cmd=( qemu-system-x86_64
	-machine q35
	-cpu core2duo
# Text mode QEMU
	-curses
# Window title in graphics mode
	-name "BareMetal"
# Boot a multiboot kernel file
	-kernel ./boot.bin
# Enable a supported NIC
	-device e1000,netdev=net0
	-netdev user,id=net0
# Amount of CPU cores
	-smp 2
# Amount of memory in Megabytes
	-m 256
# Disk configuration
#	-device ahci,id=ahci
#	-drive id=disk0,file="disk0.img",if=none,format=raw
#	-device ide-drive,drive=disk0,bus=ahci.0
#	-drive id=disk1,file="disk1.img",if=none,format=raw
#	-device ide-drive,drive=disk1,bus=ahci.1
# Ouput network to file
#	-net dump,file=net.pcap
# Output serial to file
	-serial file:serial.log
# Enable monitor mode
#	-monitor telnet:localhost:8086,server,nowait
# Enable GDB debugging
	-s
# Wait for GDB before starting execution
#	-S
)

#execute the cmd string
"${cmd[@]}"
