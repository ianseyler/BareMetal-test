; dump-pci.asm -- Output PCI device information

BITS 64
ORG 0x001E0000

%include 'api/libBareMetal.asm'

start:
	xor edx, edx			; Start with PCI device 0,0

again:
	push rdx			; Save the PCI device

	mov rcx, pci_read		; PCI read
	call [b_system_config]

	cmp eax, 0xFFFFFFFF		; Non-existant device
	je skip				; Skip if there is no device

	mov rcx, debug_dump_rax
	call [b_system_misc]
	mov rsi, newline		; Output a newline character
	mov rcx, 1
	call [b_output]

skip:
	pop rdx				; Restore the PCI device
	add edx, 0x100			; Increment to next device
	cmp edx, 0xFFFF00
	jl again

	ret				; Return CPU control to the kernel

newline: db 10