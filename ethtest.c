// ethtest.c -- Test the sending/reveiving of Ethernet packets

// gcc -c -m64 -nostdlib -nostartfiles -nodefaultlibs -o ethtest.o ethtest.c
// gcc -c -m64 -nostdlib -nostartfiles -nodefaultlibs -o libBareMetal.o api/libBareMetal.c
// ld -T c.ld -o ethtest.bin ethtest.o libBareMetal.o

#include "api/libBareMetal.h"

void ethtool_send();
void ethtool_receive();

char packet[] = {0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xAB, 0xBA, 'T', 'h', 'i', 's', ' ', 'i', 's', ' ', 't', 'e', 's', 't', ' ', 'd', 'a', 't', 'a', ' ', 'f', 'r', 'o', 'm', ' ', 'E', 't', 'h', 'T', 'e', 's', 't', ' ', 'f', 'o', 'r', ' ', 'B', 'a', 'r', 'e', 'M', 'e', 't', 'a', 'l'};
char buffer[1500];

int running = 1, len = 0;
char key;

int main(void)
{
	b_output("EthTest: S to send a packet, Q to quit.\nReceived packets will display automatically.", 84);
	// Configure the network callback
	b_config(NETWORKCALLBACK_SET, (unsigned long int)ethtool_receive);

	while (running == 1)
	{
		key = b_input();
		key = key | 0b00100000; // Convert to lowercase
		if (key == 's')
		{
			ethtool_send();
		}
		else if (key == 'q')
		{
			running = 0;
		}
	}

	// Clear the network callback
	b_config(NETWORKCALLBACK_SET, 0);

	return 0;
}

void ethtool_send()
{
	b_output("\nSending packet.", 16);
	b_ethernet_tx(packet, 50, 0);
}

void ethtool_receive()
{
	b_output("\nReceived packet:\n", 18);
	len = b_ethernet_rx(buffer, 0);
	b_system_misc(DEBUG_DUMP_MEM, buffer, (void *)len);
}
