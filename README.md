# BareMetal Test files

Included in this repo are various test applications for BareMetal.

Keep in mind that the BareMetal kernel default output is the serial port. Running `tail -f serial.log` in the BareMetal-OS folder is a good idea.

## Instructions

For these examples we have BareMetal-OS in a directory beside BareMetal-test.

    git clone https://gitlab.com/ReturnInfinity/BareMetal-OS.git
    git clone https://gitlab.com/IanSeyler/BareMetal-test.git
    cd BareMetal-OS
    ./setup.sh
    cd ../BareMetal-test
    ./setup.sh

### Assembly

    nasm hello.asm -o hello.bin
    cp hello.bin ../BareMetal-OS/sys/
    cd ../BareMetal-OS
    ./install.sh hello.bin
    ./run.sh

### C

    gcc -c -m64 -nostdlib -nostartfiles -nodefaultlibs -o hello.o hello.c
    gcc -c -m64 -nostdlib -nostartfiles -nodefaultlibs -o libBareMetal.o api/libBareMetal.c
    ld -T c.ld -o hello.bin hello.o libBareMetal.o
    cp hello.bin ../BareMetal-OS/sys/
    cd ../BareMetal-OS
    ./install.sh hello.bin
    ./run.sh

// EOF
