; div0.asm -- Generate a division by zero exception

BITS 64
ORG 0x001E0000

%include 'api/libBareMetal.asm'

start:
	xor ecx, ecx			; Clear RCX
	div rcx				; Divide RDX:RAX by RCX
	ret				; This 'ret' will never be executed
