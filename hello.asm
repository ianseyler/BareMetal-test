; hello.asm -- Output a 'hello world' message

BITS 64
ORG 0x001E0000

%include 'api/libBareMetal.asm'

start:
	mov rsi, message		; Address of hello message
	mov rcx, 14			; Length of message
	call [b_output]			; Output the message
	ret				; Return CPU control to the kernel

message: db 'Hello, world!', 10
