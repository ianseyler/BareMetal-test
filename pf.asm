; pf.asm -- Generate a Page Fault exception

BITS 64
ORG 0x001E0000

%include 'api/libBareMetal.asm'

start:
	mov rsi, 0xFFFF8FFFFFF00000	; Load an invalid memory address
	lodsb				; (Attempt to) load a byte from unmapped memory
	ret				; This 'ret' will never be executed
