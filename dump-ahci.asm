; dump-ahci.asm -- Output AHCI disk information

BITS 64
ORG 0x001E0000

%include 'api/libBareMetal.asm'

start:
	xor edx, edx			; Start with AHCI device 0,0

next:
	mov rcx, 0x50			; Disk info read
	mov rax, driveinfo
	call [b_system_config]
	inc edx
	cmp edx, 32
	je end

	cmp eax, 0xFFFFFFFF		; Non-existant device
	je next				; Skip if there is no device

; Model number
	mov rsi, driveinfo+54		; Model number (Up to 40 chars)
	mov rcx, 20
	call stringswap
	mov rcx, 40
	call [b_output]
	mov rsi, newline		; Output a newline character
	mov rcx, 1
	call [b_output]

; Serial number
	mov rsi, driveinfo+20		; Serial number (Up to 20 chars)
	mov rcx, 10
	call stringswap
	mov rcx, 20
	call [b_output]
	mov rsi, newline		; Output a newline character
	mov rcx, 1
	call [b_output]
	
; Firmware revision
	mov rsi, driveinfo+46		; Firmware revision (Up to 8 chars)
	mov rcx, 4
	call stringswap
	mov rcx, 8
	call [b_output]
	mov rsi, newline		; Output a newline character
	mov rcx, 1
	call [b_output]

	jmp next

end:
	ret				; Return CPU control to the kernel

newline: db 10

stringswap:
	push rdi
	push rsi
	push rcx
	push rax
	mov rdi, rsi
stringswaploop:
	lodsw
	mov [rdi], ah
	mov [rdi+1], al
	add rdi, 2
	sub ecx, 1
	cmp ecx, 0
	jne stringswaploop
	pop rax
	pop rcx
	pop rsi
	pop rdi
	ret

align 16

driveinfo: