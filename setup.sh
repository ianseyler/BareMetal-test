#!/bin/sh

./clean.sh

mkdir api
cd api
git clone https://gitlab.com/ReturnInfinity/BareMetal.git
mv BareMetal/api/* .
rm -rf BareMetal/
cd ..