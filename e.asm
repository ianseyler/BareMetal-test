; e.asm -- Output the letter 'e', forever.
; Adapted from https://github.com/eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee/eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee

BITS 64
ORG 0x001E0000

%include 'api/libBareMetal.asm'

start:
	mov rsi, e_msg		; Point RSI to the message
	mov rcx, 1		; The message is 1 byte long
e_loop:
	call [b_output]		; Output the message
	jmp e_loop

e_msg: db 'e'
