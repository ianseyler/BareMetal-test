// smptest.c -- Output a test message from every available CPU

// gcc -c -m64 -nostdlib -nostartfiles -nodefaultlibs -falign-functions=16 -o smptest.o smptest.c
// gcc -c -m64 -nostdlib -nostartfiles -nodefaultlibs -o libBareMetal.o api/libBareMetal.c
// ld -T c.ld -o smptest.bin smptest.o libBareMetal.o

#include "api/libBareMetal.h"

void smp_task();

char *message = "Hello from core 0x";
char *newline = "\n";
unsigned long i = 0;
unsigned long outputlock = 0;

int main(void)
{
	for (i=0; i<256; i++)
		b_system(SMP_SET, (void *)&smp_task, (void *)i);
	smp_task();
	return 0;
}

// This code will be executed on every available CPU in the system
// The mutex is used so only one CPU can output its message at a time
void smp_task()
{
	unsigned long APIC_ID = 0;
	b_system(SMP_LOCK, (void *)outputlock, 0);
	b_output(message, 18);
	APIC_ID = b_config(SMP_GET_ID, 0);
	b_system(DEBUG_DUMP_RAX, (void *)APIC_ID, 0);
	b_output(newline, 1);
	b_system(SMP_UNLOCK, (void *)outputlock, 0);
}


