; ethtest.asm -- Test the sending/reveiving of Ethernet packets

BITS 64
ORG 0x001E0000

%include 'api/libBareMetal.asm'

start:
	mov rsi, startstring
	mov rcx, 83
	call [b_output]

	; Configure the fuction to run on network activity
	mov rax, ethtest_receiver
	mov rcx, networkcallback_set
	call [b_system_config]

ethtest:
	call [b_input]
	or al, 00100000b		; Convert to lowercase

	cmp al, 's'
	je ethtest_send
	cmp al, 'q'
	je ethtest_finish
	jmp ethtest

ethtest_finish:
	mov rax, 0
	mov rcx, networkcallback_set
	call [b_system_config]
	ret				; Return CPU control to the kernel

ethtest_send:
	mov rsi, sendstring
	mov rcx, 16
	call [b_output]
	mov rsi, packet
	mov rcx, 50
	call [b_ethernet_tx]
	jmp ethtest

ethtest_receiver:
	mov rsi, receivestring
	mov rcx, 18
	call [b_output]
	mov rdi, buffer
	call [b_ethernet_rx]
	mov rdx, rcx
	mov rax, buffer
	mov rcx, debug_dump_mem
	call [b_system_misc]
	jmp ethtest


startstring: db 'EthTest: S to send a packet, Q to quit.', 10, 'Received packets will display automatically'
newline: db 10
sendstring: db 10, 'Sending packet.'
receivestring: db 10, 'Received packet:', 10

align 16

packet:
destination: db 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF
source: db 0x00, 0x00, 0x00, 0x00, 0x00, 0x00
type: db 0xAB, 0xBA
data: db 'This is test data from EthTool for BareMetal'

align 16

buffer: db 0
