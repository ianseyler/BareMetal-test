// hello.c -- Output a 'hello world' message

// gcc -c -m64 -nostdlib -nostartfiles -nodefaultlibs -o hello.o hello.c
// gcc -c -m64 -nostdlib -nostartfiles -nodefaultlibs -o libBareMetal.o api/libBareMetal.c
// ld -T c.ld -o hello.bin hello.o libBareMetal.o

#include "api/libBareMetal.h"

int main(void)
{
	b_output("Hello, world!\n", 14);
	return 0;
}
