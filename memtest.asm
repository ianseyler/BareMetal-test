; memtest.asm -- Write to all available memory

BITS 64
ORG 0x001E0000

%include 'api/libBareMetal.asm'

start:
	xor ebx, ebx
	mov rcx, 8			; Free memory in MiB
	call [b_system_misc]
	mov rdx, rax

	mov rdi, 0xFFFF800000000000

nextpage:
	mov rax, rdi
	mov rcx, 5			; Dump RAX
	call [b_system_misc]
	mov rsi, newline
	mov rcx, 1
	call [b_output]
	mov rax, 0x55AA55AA55AA55AA
	mov rcx, 262144
	rep stosq
	add rbx, 2
	cmp rbx, rdx
	jne nextpage

	ret				; Return CPU control to the kernel

newline: db 10