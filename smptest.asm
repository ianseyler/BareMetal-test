; smptest.asm -- Output a test message from every available CPU

BITS 64
ORG 0x001E0000

%include 'api/libBareMetal.asm'

start:
	mov rcx, smp_set	; API Code
	mov rax, smp_task	; Code for CPU to run
	xor edx, edx		; Start at ID 0
startloop:
	call [b_system]		; Give the CPU a code address
	add edx, 1		; Increment to the next CPU
	cmp rdx, 256		; Set a maximum of 256 CPUs
	jne startloop
	call smp_task		; Call the code on this CPU as well
	ret			; Return CPU control to the kernel

align 16

; This code will be executed on every available CPU in the system
; The mutex is used so only one CPU can output its message at a time
smp_task:
	mov rax, outputlock	; Location of the mutex
	mov rcx, smp_lock	; Aquire the lock
	call [b_system]
	mov rsi, message	; Output the "Hello..." message
	mov rcx, 18
	call [b_output]
	mov rcx, smp_get_id	; Get the APIC ID of the CPU
	call [b_config]
	mov rcx, debug_dump_rax	; And then dump is a a hexidecimal number
	call [b_system]
	mov rsi, endmessage	; Output a newline
	mov rcx, 1
	call [b_output]
	mov rax, outputlock
	mov rcx, smp_unlock	; Release the mutex
	call [b_system]
	ret

message: db 'Hello from core 0x'
endmessage: db 10
outputlock: dq 0
